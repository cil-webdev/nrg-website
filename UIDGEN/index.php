<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UIDGEN (test)</title>
</head>

<body>
<?php 
// DB connection string and global functions
require_once($_SERVER['DOCUMENT_ROOT']."/UIDGEN/login.php"); 
require_once($_SERVER['DOCUMENT_ROOT']."/UIDGEN/functions.php"); 

// read last UID from database
$q = "SELECT * FROM nrg_UID ORDER BY timestamp DESC LIMIT 1"; 
$r = mysqli_fetch_array(mysqli_query($db,$q)) or die ($q); 

// echo "<p>".$q."</p>";
// echo "<p>".$r['UID']."</p>";

// split the final digits from the end and increment by one
$uid = explode(".",$r['UID']); 
$uidTail = $uid[count($uid)-1]; 
$newUidTail = intval($uidTail) + 1; 

// generate new UID using the root of the old one. 
$newUID = array(); 
foreach ($uid as $key => $value) : 
	if ($key != (count($uid)-1)) : 
		$newUID[] = $value; 
	else : 
		$newUID[] = $newUidTail; 
	endif; 
endforeach;
$newUIDstring = implode(".",$newUID);

// get user's IP address
$ip = get_ip_address(); 
?>
<script>
console.log("IP:","<?php echo $ip ?>"); 
</script>

<?php
// place the new UID in the database and display it on screen
$q = "INSERT INTO nrg_UID (UID,UserIP) VALUES ('".$newUIDstring."','".$ip."');"; 
$r = mysqli_query($db,$q) or die($q); 
?>

<pre style="word-wrap: break-word; white-space: pre-wrap;">{"UID":"<?php echo $newUIDstring ?>"}</pre>

<?php
// close DB connection
mysqli_close($db); 
?>

</body>
</html>