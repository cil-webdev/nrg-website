<div class="anythingSlider">      
	<div class="wrapperSlider">
    	<ul>
            <?php query_posts('cat=9&orderby=title&order=ASC'); ?>
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
   				<li class="<?php the_title(); ?>">  <?php the_content(); ?> </li>
     		
     		<?php endwhile; ?>
            <?php rewind_posts(); ?>
            
		</ul>        
	</div>
</div>    
  
  