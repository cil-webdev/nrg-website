<?php
/**
 * Template Name: Software Page
 *
 * This template is customized for software display.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    
<?php require_once("includes/contextual_nav.php"); ?>

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">
		
	<?php require_once("includes/breadcrumbs.php") ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

        <div id="subpage">
            <h2><?php the_title(); ?></h2>
            <?php $meta = get('metadata_link'); ?>

            <div class="entry-content" <?php if ($meta) { echo "style=\"max-width: 420px;\""; } ?>>
                <?php the_content(); ?>
            </div>
            
            <?php if($meta) : ?>
                <div class="project_right">
                    <img src="<?php echo get('project_image'); ?>" alt="" />
                    <h3>Overview</h3>
                    <div class="project_overview">
						<?php $meta_safe = "metadata/" . strtolower(str_replace(array("."," "),"-",$meta)); ?>
						<ul>	
							<?php $args2 = array('pagename' => $meta_safe, 'showposts' => 1);
                            $my_query = new WP_Query($args2); 
                            while ($my_query->have_posts()) : $my_query->the_post(); ?>
                                <li><strong>Developer(s):</strong> <?php echo get('Developer'); ?></li>
                                <li><strong>Version:</strong> <?php echo get('Version'); ?></li>
                                <li><strong>Release Date:</strong> <?php if(get('release_date2')) { echo get('release_date2'); } else { echo "TBA"; } ?></li>
                                <li><strong>Development Status:</strong> <?php echo get('Status'); ?></li>
                                <li><strong>Language:</strong> <?php echo get('Language'); ?></li>
                                <li><strong>Additional Requirements:</strong> <?php echo get('Additional_Requirements'); ?></li>
                                <li><strong>Link to Source:</strong> <a href="<?php echo get('Source_Code'); ?>"><?php echo get('Source_Code'); ?></a></li>
                                <li><strong>Website:</strong> <a href="<?php echo get('basic_info_website'); ?>"><?php echo get('basic_info_website'); ?></a></li>
                        	<?php endwhile;  ?>
                        </ul>			
                    </div>
                </div>
            <?php endif; ?>
        
        </div><!-- #subpage -->
	
	<?php endwhile; ?>
	
	<?php get_footer(); ?>
