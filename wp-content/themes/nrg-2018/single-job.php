<?php
/**
 * Template Name Posts: Jobs
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="page_side">
	<?php foreach((get_the_category()) as $category) { } ?>
    <h2 style="text-transform: capitalize">
        <a href="/jobs/">Jobs at NRG</a></h2>
         <?php $title = get_the_title($post->ID); ?>
		 	<?php
		$ego = $post->ID;

		$args_now = array(
			'category_name' => 'jobs',
			'post__not_in' => array(1102,2202,2244,2815),
			'order' => DESC,
			'posts_per_page' => '3', // shows 3 most recent items
		); 
  	?>
    <ul>
	 	<li><strong>NOW HIRING:</strong></li>
		<?php 
		$my_query = new WP_Query($args_now);  
        $postCount=5; // change this number to change the number of posts displayed.
        
		while ( ($my_query->have_posts() && ($postCount>0)) ) : $my_query->the_post(); ?>
		<li>
        	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
        </li>
        <?php endwhile; ?>
        <li style="border-top:1px dotted #e0e0e0; padding:7px 0 0 !important; margin-top:7px;"><a href="/jobs/">All Jobs</a></li>
    </ul>
</div> <!-- /#page_side nav -->

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<?php require_once("includes/breadcrumbs-post.php") ?>
	
	<div id="subpage">
	
        <div id="post_single">
          <h1><?php the_title(); ?></h1>
                  
          <div class="entry-content">
              <h3>Job Description</h3>
              <?php echo get('description'); ?>
    
              <h3>Job Requirements</h3>
              <?php echo get('requirements'); ?>
    
              <?php if (get('contact')) : ?>
                  <h3>For more info, contact</h3>
                  <?php echo get('contact'); ?>
              <?php endif; ?>
              <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                  
              <?php 
              $title = get_the_title($post->ID);
              $safe_title = strtolower($title);
              $page = get_page_by_title($title);
              if ($page->post_title == $title) { ?>
                  
                  <a href="<?php bloginfo('url'); ?>/project-index/<?php echo $safe_title; ?>">
                              Click here to find out more about <?php the_title(); ?></a>
                                              
                                          <?php } ?>
		  
          </div><!-- .entry-content -->
      </div><!-- #post-## -->

	<?php endwhile; // end of the loop. ?>
    
    </div> <!-- /#subpage -->
	
	<?php get_footer(); ?>

