<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header();

?>
<?php 
//get the category of the current page, including the ID which is set as $cat_number 
$the_cat = get_the_category($post->ID); foreach($the_cat as $cat_num) { 
$cat_number = $cat_num->cat_ID; 
$cat_parent = $cat_num->category_parent;
$cat_count = $cat_num->category_count;
$cat_name = $cat_num->cat_name;
$cat_nicename = $cat_num->category_nicename; } 
$the_cat_parent = get_the_category($cat_parent); foreach ($the_cat_parent as $cat_parent2) { $cat_parent_count = $cat_parent2->category_count; }
?>

<div id="page_side">

<?php $status = $_GET['status'];  ?>
<?php if(is_null($status)) { $status = "upcoming"; } ?> 
	<h2><a href="<?php echo bloginfo('url') . "/" . $cat_nicename; ?>"><?php echo $cat_name; ?></a></h2>
	
	<ul>	 
		<li <?php if($status=="upcoming") { echo 'class="current_page_item"'; } ?>>
			<a href="<?php bloginfo('url'); ?>/category/<?php echo $cat_nicename; ?>/?status=upcoming" title="upcoming_events">Upcoming</a>
		</li>
		<li <?php if($status=="past") { echo 'class="current_page_item"'; } ?>>
			<a href="<?php bloginfo('url'); ?>/category/<?php echo $cat_nicename; ?>/?status=past" title="past_events">Past</a>
		</li>
		<li <?php if($status=="all") { echo 'class="current_page_item"'; } ?>>
			<a href="<?php bloginfo('url'); ?>/category/<?php echo $cat_nicename; ?>/?status=all" title="all_events">All</a>
		</li>
	</ul>
</div>
  
<?php require_once("includes/sidebar_links.php"); ?>	
  			
<div id="right">

    <div id="breadcrumbs" style="margin-left: 10px;">
        <a href="<?php bloginfo('url'); ?>">Home</a> 
        &raquo; <?php $get_cat_parents = get_category_parents($cat_num, TRUE, ' &raquo; '); 
            if(strstr($get_cat_parents, "Object")) { single_cat_title(); } else {  //if there are no posts, this Object id crap will appear. this hacks it away.
                echo $get_cat_parents; } ?>  
        <?php $types = array("Jobs", "News", "Events"); ?>
        <?php if($status) { echo ucwords($status); } ?>
        <?php //if(!in_array(single_cat_title("", false), $types)) { single_cat_title("&raquo; ", true); } ?>
    </div>
		
				
	<div id="subpage">	

	<?php
    	// get offset from URL, if defined 
		$offset = $_GET['offset'];
		?>
        
	<?php
		// get today's date and correct for timezone & daylight savings
		$today = getdate();
		$timestamp = strtotime( date("M d, Y g:i a",$today). " UTC");
	?>
        <!-- Showing events as of <?php echo date("M d, Y g:i a",$timestamp-21600) ?> (US Central Time) -->
    <?php 
		if ($status != 'past') : 
			// query for upcoming events
			$future_query = new WP_Query(array(
						'order' => 'ASC', 
						'meta_key' => 'Date', 
						'meta_compare'=> '>=',
						'meta_value' => date("Y-m-d"),
						'post__not_in' => array(1102))); 
			if ($future_query->have_posts()) : 
			?>
            	<h2>Upcoming Events</h2>
            	<?php while ($future_query->have_posts()) : $future_query->the_post(); ?>
                	<?php if ( strtotime(get('Date')) > $timestamp ) : ?>
                        <div class="entry-module" id="<?php the_title(); ?>">
                            <p><span class="sidebar-nav-date"><?php echo get('Date') ?>, <?php echo get('Time') ?></span></p>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>		
                            <p><?php the_excerpt(); ?></p>
                        </div>
                    <?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
				    <div class="entry-module"><h3>There are no upcoming events at this time.</h3> </div>
			<?php endif; ?> 
		<?php
        endif; 
		wp_reset_query();
		
		// get previous posts
		$past_query = new WP_Query(
			array(
				'cat'=>$cat_number,
				'meta_query'=>array(
					'key' => 'Date',
					'value' => date("Y-m-d",$timestamp-21600),
					'type' => 'DATE',
					'compare' => '<'
				),
				'offset'=>$offset
			)
		); 
		if ($past_query->have_posts()) : ?>
        	<h2>Previous Events</h2>
            <?php if ($offset) : 	?>
        	<p>Posts offset by: <?php echo $offset ?></p>
       		<?php endif; ?>
            <?php while ($past_query->have_posts()) : $past_query->the_post(); ?>
                <?php if ( strtotime(get('Date')) < $timestamp ) : ?>
                    <div class="entry-module" id="<?php the_title(); ?>">
                        <p><span class="sidebar-nav-date"><?php echo get('Date') ?>, <?php echo get('Time') ?></span></p>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>		
                        <p><?php the_excerpt(); ?></p>
                    </div>
                <?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>
	
	<?php
    /* Run the loop for the tag archive to output the posts
     * If you want to overload this in a child theme then include a file
     * called loop-tag.php and that will be used instead.
     */
    // get_template_part( 'loop', 'category' );
    ?>
    
    <?php if (  $past_query->max_num_pages > 1 ) : ?>
        <div id="nav-below" class="navigation">
            <div class="nav-previous"><a href="?status=past&offset=<?php echo $offset+10 ?>">Earlier Events</a></div>
        </div><!-- #nav-below -->
	<?php endif; ?>
	</div>
	

<?php get_footer(); ?>
		
	
		