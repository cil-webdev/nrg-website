<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="page_side">
			<?php foreach((get_the_category()) as $category) { } ?>
			<h2 style="text-transform: capitalize">
				<a href="<?php echo bloginfo('url') . "/" . $category->category_nicename; ?>"><?php echo $category->category_nicename; ?></a></h2>
				 <?php $title = get_the_title($post->ID); ?>
	<?php
		$ego = $post->ID;
		if($category->category_nicename == 'events') { 
			$args_now = array(
  				'category_name' => $category->category_nicename,
  				'post__not_in' => array(55, 57, 59, 61),
  				'post_status' => future,
  				'order' => DESC
  				);
 		} else { 
 			$args_now = array(
  				'category_name' => $category->category_nicename,
  				'post__not_in' => array(55, 57, 59, 61),
				'post_status' => array(published, future),
  				'order' => DESC 
  				);
		 } ?>
  
 
  
 <ul>
   	<?php $my_query = new WP_Query($args_now);   
  	 while ($my_query->have_posts()) : $my_query->the_post(); ?>
	 
   		<li <?php if($title == get_the_title($post->ID)) { echo 'class="current_page_item"'; } ?>>
   			<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
   		</li>   	
  
  	<?php endwhile; ?>

</ul>

</div>

<?php require_once("includes/sidebar_links.php"); ?>	
		
		<div id="right">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <?php require_once("includes/breadcrumbs-post.php"); ?>

				<?php if(!in_category("Jobs")) { ?>
                        <div id="subpage" class="post_single">
                            <h1><?php the_title(); ?></h1>
                            <div class="entry-meta">
                                <?php if(in_category("Events") && !is_single('events')) { the_time('l, M j, Y, g:i a'); } ?>
                            </div><!-- .entry-meta -->
                            
                            <div class="entry-content">
                                <?php the_content(); ?>
                                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                            </div><!-- .entry-content -->
        
                <?php } else { ?>
                        <div id="subpage" class="post_single">
                            <h1><?php the_title(); ?></h1>
                                    
                            <div class="entry-content">
                            <h3>Job Description</h3>
                                <?php echo get('description'); ?>
                            <h3>Job Requirements</h3>
                                <?php echo get('requirements'); ?>
                            <h3>For more info, contact</h3>
                                <?php echo get('contact'); ?>
                                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                
                <?php } ?>			
				
            </div><!-- #post-## -->

		
	<?php endwhile; // end of the loop. ?>

			
			
	<?php get_footer(); ?>



