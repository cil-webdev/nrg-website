<?php
/**
 * Template Name: Project/Software Index Page
 *
 * This template is customized for project display.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    
<?php require_once("includes/sidebar_nav.php"); ?>

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">
		
		<?php if(is_page() && !$post->post_parent) { ?>
			<div id="breadcrumbs" style="margin-left: 10px;">
				<a href="<?php bloginfo('url'); ?>">Home</a> &raquo; <?php the_title(); ?>
			</div>
		<?php } ?>
	
		<?php 
		$this_title = get_the_title();
	if(is_page("Projects")) { ?>
		<div id="index_header">
			<?php the_content(); ?>
		</div>
		<?php query_posts(array('showposts' => -1, 'post_parent' => 709, 'post_type' => 'page', 'orderby' => 'menu_order', 'order' => 'ASC')); } 
	
	elseif(is_page("Software")) { ?>
		<div id="index_header">
			<?php the_content(); ?>
		</div>
		<?php query_posts(array('showposts' => -1, 'post_parent' => 650, 'post_type' => 'page', 'orderby' => 'menu_order', 'order' => 'ASC')); } ?>
	
		<?php while (have_posts()) : the_post(); ?>
	
			<div class="index_module">
 				<div class="index_module_left">
					<?php	if(get_image('icon')) { ?>
  					<a href="<?php the_permalink(); ?>"><?php echo get_image('icon'); ?></a> <?php } 
  						else { ?>
  							<a href="<?php the_permalink(); ?>">
  								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/<?php echo $this_title; ?>_icon.jpg" alt="<?php the_title(); ?>" />
  							</a>
  					<?php } ?>
  			</div>
  			<div class="index_module_right">	
  			<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>	
  				<?php if($this_title == "Projects") { the_excerpt(); }
  					  elseif($this_title == "Software") { ?> 
  					  
  					  <ul>
									<?php $meta2 = get('metadata_link'); 
									 if($meta2) { 
											$meta2_safe = "metadata/" . strtolower(str_replace(array("."," "),"-",$meta2));
											
								$args21 = array('pagename' => $meta2_safe, 'showposts' => 1);
								$my_query = new WP_Query($args21); 
									while ($my_query->have_posts()) : $my_query->the_post(); ?>
  									
										<li><strong>Version:</strong> <?php echo get('Version'); ?></li>
										<li><strong>Status:</strong> <?php echo get('Status'); ?></li>
										<li><strong>Release Date:</strong> <?php if(get('release_date2')) { echo get('release_date2'); } else { echo "TBA"; } ?></li>
								<?php endwhile;  ?>
									</ul>	
  					<?php  } }  ?>
  			</div>
 	</div>
	<?php endwhile; ?>
    <?php wp_reset_query(); ?>
	
	<?php get_footer(); ?>
