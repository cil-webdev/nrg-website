<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
	<?php 
        //get the category of the current page, including the ID which is set as $cat_number 
        $the_cat = get_the_category($post->ID); foreach($the_cat as $cat_num) { 
        $cat_number = $cat_num->cat_ID; 
        $cat_parent = $cat_num->category_parent;
        $cat_count = $cat_num->category_count;
        $cat_name = $cat_num->cat_name;
        $cat_nicename = $cat_num->category_nicename; } 
        $the_cat_parent = get_the_category($cat_parent); foreach ($the_cat_parent as $cat_parent2) { $cat_parent_count = $cat_parent2->category_count; }
    ?>

	<div id="page_side">
		<h2><a href="/<?php echo $cat_nicename; ?>"><?php echo $cat_name; ?></a></h2>
		<ul>
		<?php $categories = get_categories('child_of='.$cat_number); ?>
        	<li><a href="/<?php echo $cat_nicename ?>">All (<?php echo $cat_count ?>)</li>
            <?php foreach($categories as $category) : ?>
				<li><a href="/category/<?php echo $category->category_nicename ?>"><?php echo $category->cat_name ?> (<?php echo $category->category_count ?>)</a></li>
			<?php endforeach; ?>
        </ul>	
	</div>
  
  	<?php require_once("includes/sidebar_links.php"); ?>
  			
	<div id="right">

        <div id="breadcrumbs" style="margin-left: 10px;">
            <a href="<?php bloginfo('url'); ?>">Home</a> 
            &raquo; <?php $get_cat_parents = get_category_parents($cat_num, TRUE, ' &raquo; '); 
                if(strstr($get_cat_parents, "Object")) { single_cat_title(); } else {  //if there are no posts, this Object id crap will appear. this hacks it away.
                    echo $get_cat_parents; } ?>  
        </div>
				
		<div id="subpage">	
        	<h2><?php single_cat_title(); ?></h2>
		
        <?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
                <div class="entry-module">
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php if(in_category("News")) { ?>
                        <p><span class="sidebar-nav-date"> <?php the_date(); ?></span></p>
                        <p><?php echo get_the_excerpt(); ?></p>
                    <?php } elseif(in_category("Jobs")) { ?>
                        <?php echo get('description'); ?>
                    <?php } else { ?>
                        <p><?php echo get_the_excerpt(); ?></p>
                    <?php } ?>
                </div>
             <?php endwhile; ?>
            
        <?php else : 
            if(is_category("Jobs")) { ?>
                <div class="entry-module"><p>There are no available job listings at this time.</p> </div>
            <?php } if(is_category("News")) { ?>
                <div class="entry-module"><p>There is no recent news at this time.</p> </div>
            <?php } else { ?>
            	<div class="entry-module"><p>There are no posts in this category.</p></div>
            <?php } 
		endif; ?>
                
        </div>

<?php get_footer(); ?>
		
	
		