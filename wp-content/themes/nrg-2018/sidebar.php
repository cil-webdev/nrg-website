<?php
/**
 * The "Sidebar" exists only on the home page, and profiles recent posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div id="modules">
		<div class="module">
			<h1><a href="<?php bloginfo('url'); ?>/category/events">EVENTS</a></h1><br />

				<?php   	 
					$my_query = new WP_Query(array(
						'order' => 'ASC', 
						'showposts' => '4', 
						'meta_key' => 'Date', 
						'meta_compare'=> '>=',
						'meta_value' => date("Y-m-d"),
						'post__not_in' => array(1102)));   
  					if ($my_query->have_posts()) : 
						while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<?php 					
                            $event_time = strtotime(get('Date'));
                            $current_time = time();
                                        
                            if(($event_time + 86000) >= $current_time) : ?>
				
						 
                                <div class="post_events">
                                    <span class="sidebar-nav-date"><?php $dateline = get('Date'); echo $dateline ?></span>
                                    <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                                </div>
							
							<?php endif; ?>
   						<?php endwhile; ?>
                    <?php else : ?>
   						<h3>There are no upcoming events at this time.</h3> 
   						Click <a href="<?php bloginfo('url'); ?>/category/events?status=all">here</a> to view past events. 
   					<?php endif; ?>
   					
   					<?php if(isset($dateline)) { ?>
   						<h4 style="float: right;"><a href="<?php bloginfo('url'); ?>/category/events">...More Events</a></h4>
   					<?php } ?>
   			
		</div>


		<div class="module">
			<h1><a href="<?php bloginfo('url'); ?>/category/news">NEWS</a></h1><br />

				<?php
					$my_query = new WP_Query('category_name=news&posts_per_page=4');   
  					while ($my_query->have_posts()) : $my_query->the_post(); ?>
						 
		   					<div class="post_news">
							<span class="sidebar-nav-date"><?php the_time('F j, Y'); ?></span>
							<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							<?php /* the_excerpt(); */ ?>
		   				</div>
					
   					<?php endwhile; ?>
   			<h4 style="float: right;"><a href="<?php bloginfo('url'); ?>/category/news">...More News</a></h4>
		</div>


		<div class="module">
		  <h1> <a href="<?php bloginfo('url'); ?>/category/jobs">JOBS</a></h1><br />

				<?php
					$my_query = new WP_Query('category_name=jobs&posts_per_page=4');   
  					while ($my_query->have_posts()) : $my_query->the_post(); ?>
						 
		   					<div class="post_jobs">
								<span class="sidebar-nav-date"><?php the_time('F j, Y'); ?></span>
								<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<!-- <?php echo substr(get('description'), 0, 180); ?>...[<a href="<?php the_permalink(); ?>">More</a>] -->
		   					</div>
		     			<?php endwhile; ?>
		  <h4 style="float: right;">
		  	 <?php if(get('description')) { ?> 
		  	 <a href="<?php bloginfo('url'); ?>/category/jobs">...More Job Information</a> <?php } else { ?>There are no available job listings at this time. <?php } ?>
		  </h4>
		</div>
</div>