<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<div id="footer">
	
		<?php	get_sidebar( 'footer' ); ?>
		
			<div id="site-info">

			<?php //do_action( 'twentyten_credits' ); ?>
				<!--Powered by<a href="<?php echo esc_url( __('http://wordpress.org/', 'twentyten') ); ?>"
						title="<?php esc_attr_e('Semantic Personal Publishing Platform', 'twentyten'); ?>" rel="generator">
					<?php //printf( __(' %s', 'twentyten'), 'WordPress' ); ?> | 
				</a> -->
				<a href="<?php bloginfo('url'); ?>/sitemap">Site Map</a> |
				<a href="<?php bloginfo('url'); ?>/privacy">Privacy</a> |
				&#0169; 2010-<?php echo date('Y',time()) ?> <a href="<?php bloginfo('url'); ?>/">Neuroinformatics Research Group</a> | <a href="mailto:webmaster@nrg.wustl.edu?subject=Bug Report: <?php echo get_the_title() ?>">Report a bug</a>
				<br />


			</div><!-- #site-info -->

			<div id="site-generator">
				
				The NRG site Network: 
				<a href="http://nrg.wustl.edu">NRG</a>|
				<a href="http://xnat.org">XNAT.org</a>|
				<a href="http://humanconnectome.org/">Human Connectome Project</a>
			</div><!-- #site-generator -->

	</div><!-- #footer -->



<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</div><!--#main-->
</div><!--#wrapper-->
</div>

<!--<div id="scroll_forever"></div>-->

</body>
</html>
