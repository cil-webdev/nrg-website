<?php
/**
 * Template Name Posts: News
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div id="page_side">
	<?php foreach((get_the_category()) as $category) { } ?>
    <h2 style="text-transform: capitalize">
        <a href="/news/">NRG News</a></h2>
         <?php $title = get_the_title($post->ID); ?>
		 	<?php
		$ego = $post->ID;

		$args_now = array(
			'category_name' => 'news',
			'post__not_in' => array(1102,2202,2244,2815),
			'order' => DESC,
			'posts_per_page' => '3', // change this number to change the number of posts displayed
		); 
  	?>
    <ul>
		<?php 
		$my_query = new WP_Query($args_now);  
        
		while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
		<li <?php if($title == get_the_title($post->ID)) { echo 'class="current_page_item"'; } ?>>
        	<span class="sidebar-nav-date"><?php the_date('F j, Y'); ?></span> 
   			<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
   		</li>
        <?php endwhile; ?>
        <li style="border-top:1px dotted #e0e0e0; padding:7px 0 0 !important; margin-top:7px;"><a href="/news/">All News</a></li>
    </ul>
</div> <!-- /#page_side nav -->

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">

			
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<?php require_once("includes/breadcrumbs-post.php") ?>
        <div id="subpage">	
            <div id="post_single">
              <h1><?php the_title(); ?></h1>
              <div class="entry-meta">
                <?php the_date('F j, Y'); ?>
              </div><!-- .entry-meta -->          
              <div class="entry-content">
                 <?php the_content(); ?>
              </div><!-- .entry-content -->
          </div><!-- #post-## -->

	<?php endwhile; // end of the loop. ?>
    
    </div> <!-- /#subpage -->
	
	<?php get_footer(); ?>

