<div class="thumbNav_pages">
              	<?php 
					$ancestors2 = get_post_ancestors($post->ID);
   			 		$reverse2 = array_reverse($ancestors2);
   			 		$correct2 = $reverse2[0];
   			 		$project_parent = $reverse2[1];
   			 		$titulo = get_the_title($project_parent);
			    ?>
          
	<ul>
	
				<?php $args=array(
					'post_parent' => "709",
					'orderby' => 'date',
					'order' => 'ASC',
					'post_type' => 'page',
					'showposts' => '-1'
	
				); ?>
			   	
		<?php $my_query = new WP_Query($args);   
  	 		if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
   					
   					<li class="<?php the_title(); ?>"> 
   						<?php 
   							$title = get_the_title($post->ID);
   							$safe_title = str_replace("tab_", "", get_the_title());
   						 	$secure_title = strtolower($title); 
   						 ?>
   						<?php if($titulo != $title) { ?>
   							<a href="<?php bloginfo('url'); ?>/projects/<?php echo $secure_title; ?>" style="background: <?php echo get('project_color'); ?> url('images/tab_shadow1.png') bottom no-repeat;">
   							<img src="<?php echo get('project_tab_label'); ?>" alt="<?php $title; ?>" />
   							</a>
   						<?php } if ($titulo == $title) { ?>
   							<a href="<?php bloginfo('url'); ?>/projects/<?php echo $secure_title; ?>" style="background-color: #ffffff">
   							<img src="<?php echo get('project_tab_label'); ?>" alt="<?php $title; ?>" />
   							</a>
   						<?php } ?>
   						
   					</li> 
   						
   						
     	<?php endwhile; ?>
     	<?php else: endif; ?>
     	<?php wp_reset_query(); ?>
	</ul>	
          
</div>