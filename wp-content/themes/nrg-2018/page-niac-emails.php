<?php
/**
 * Template Name: NIAC Email Subscribe
 *
 * The template for displaying photo arrays
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
      
<?php require_once("includes/contextual_nav.php"); ?>

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">
		
	<?php require_once("includes/breadcrumbs.php") ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
        <div id="subpage">

            <div class="entry-content" style="max-width: 700px">
            
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
                
                <form action="http://wusmlist.wustl.edu/subscribe/subscribe.tml" method="POST">
                    <table border=0 cellspacing=0 cellpadding=3>
                        <tr>
                            <td align="right">
                                <font size=1>Email address: </font>
                            </td>
                            <td>
                                <input type="text" name="email" value="" SIZE=10>
                            </td>
                            <td>
                            <input type="submit" class="btn" value="subscribe" title="subscribe" alt="subscribe"  name="subscribe" align="left">
                            </td>
                        </tr>
                    <tr>
                        <td align="right">
                            <font size=1><I>(optional)</I> Your name: </font>
                        </td>
                        <td>
                            <INPUT TYPE="text" NAME="name" VALUE="" SIZE=10 MAXLENGTH=100>
                        </td>
                    </tr>
                    </table>
                    <input type="hidden" name="list" value="niac-seminars" >
                    <input type="hidden" name="demographics" value="" >
                    <input type="hidden" name="name_required" value="" >
                    <input type="hidden" name="pw_required" value="" >
                    <input type="hidden" name="confirm" value="one" >
                    <input type="hidden" name="showconfirm" value="T" >
                    <input type="hidden" name="url" value="" >
                    <input type="hidden" name="appendsubinfotourl" value="" >
                    <input type="hidden" name="secx" value="d10b9ddd" >
            
                </form>
            
                <h3>Unsubscribe Form</h3>
                If you are already on our email list and would like to stop receiving these updates, you may unsubscribe here: 
                <form action="http://wusmlist.wustl.edu/subscribe/unsubscribe.tml" method="GET">
                    <table border=0 cellspacing=0 cellpadding=3>
                        <tr>
                            <td>
                                <font size=1>Email address: </font>
                            </td>
                            <td>
                                <input type="text" name="email" value="" size=10>
                            </td>
                            <td>
                                <input type="submit" class="btn" value="unsubscribe" title="unsubscribe" alt="unsubscribe"  name="unsubscribe">
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="lists" value="niac-seminars">
                    <input type="hidden" name="url" value="" >
                    <input type="hidden" name="appendsubinfotourl" value="T" >
                    <input type="hidden" name="email_notification" value="T" >
                    <input type="hidden" name="confirm_first" value="F" >
                    <input type="hidden" name="showconfirm" value="T" >
                    <input type="hidden" name="secx" value="c48e3a8e" >
                </form>
                            
            </div>
        </div>
	
	<?php endwhile; ?>
    
	<?php get_footer(); ?>
