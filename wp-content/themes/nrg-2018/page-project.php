<?php
/**
 * Template Name: Project Page
 *
 * This template is customized for project display.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    
<?php require_once("includes/contextual_nav.php"); ?>

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">
		
	<?php require_once("includes/breadcrumbs.php") ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

        <div id="subpage">
            <h2><?php the_title(); ?></h2>
            
            <div class="entry-content" <?php if (get('overview')) { echo "style=\"max-width: 420px;\""; } ?>>
                <?php the_content(); ?>
            </div>
            
            <?php if(get('overview')) : ?>
                <div class="project_right">
                    <img src="<?php echo get('project_image'); ?>" alt="" class="project_image" />
                    <h3>Overview</h3>
                    <div class="project_overview">
	                    <?php echo get('overview'); ?>
                    </div>
                </div>
            <?php endif; ?>
        
        </div><!-- #subpage -->
	
	<?php endwhile; ?>
	
	<?php get_footer(); ?>
