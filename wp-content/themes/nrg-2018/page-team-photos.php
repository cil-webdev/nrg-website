<?php
/**
 * Template Name: Staff Page
 *
 * The template for displaying photo arrays
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
      
<?php require_once("includes/contextual_nav.php"); ?>

<?php require_once("includes/sidebar_links.php"); ?>	

<div id="right">
		
	<?php require_once("includes/breadcrumbs.php") ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
        <div id="subpage">

            <div class="entry-content" style="max-width: 700px">
            
                <h1><?php the_title(); ?></h1>
                <div class="team-photo-array">
                    <?php the_content(); ?>
                </div>
                            
            </div>
        </div>
	
	<?php endwhile; ?>
    
	<?php get_footer(); ?>
