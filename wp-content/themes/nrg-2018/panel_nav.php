    <div class="thumbNav_container">
    	<div class="thumbNav_pages">
            <ul>
	              	<?php 
					$ancestors2 = get_post_ancestors($post->ID);
   			 		$reverse2 = array_reverse($ancestors2);
   			 		$correct2 = $reverse2[0];
   			 		$project_parent = $reverse2[1];
   			 		$titulo = get_the_title($project_parent);
			    	?>
			    	
					<?php $args2=array(
						'post_parent' => "709",
						'orderby' => 'menu_order',
						'order' => 'ASC',
						'post_type' => 'page',
						'showposts' => '-1'
					); ?>
	
			   	 <?php $my_query = new WP_Query($args2);   
  	 if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
   						 	
   				<?php $titler = str_replace(" ", "_", get_the_title($post->ID)); ?>
   				
   					<li class="<?php echo $titler ?>"> 
   					
   						<?php $safe_title = str_replace("tab_", "", get_the_title()); ?>
   						<a href="javascript:paneswitch('<?php echo $titler; ?>');" style="background-color:
   						<?php if($titulo == $titler) { 
   							echo "#ffffff\""; 
   								} else { 
   									echo get('project_color'); ?>" class="shadow tab" <?php } ?>
   						  id="<?php echo $titler; ?>_tab" >
   							<img src="<?php echo get('project_tab_label'); ?>" alt="" />
   						</a>
   					</li> 
   						
   						
     		<?php endwhile; ?>
     		<?php else: endif; ?>
     		<?php wp_reset_query(); ?>
          </ul>	
        </div> 
 </div>
 <div id="panel">         
     <?php $my_query = new WP_Query($args2);   
  	 if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>
  	    					
  	    					<?php 
   							$title = get_the_title($post->ID);
   							$safe_title = str_replace("tab_", "", get_the_title());
   						 	$secure_title = str_replace(" ", "_", strtolower($title)); 
   						 	?>
   						 	
          <div id="<?php echo str_replace(" ", "_", get_the_title($post->ID)); ?>" class="pane" style="display: none; background-color: <?php echo get('project_color'); ?>;">
          	  <div class="pane_content">
          	  	<a href="javascript:panelclose();" class="close"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/hide.png" alt="Hide" /></a>
          	  
          		<h2 class="selective"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          		<?php the_excerpt(); ?>
          		
          	 </div>
          	 <img src="<?php echo get('project_image'); ?>" alt="" />
             <div class="more_info" style="background-color: <?php echo get('project_color'); ?>;">
                <h5><a href="<?php the_permalink(); ?>">MORE ABOUT THIS PROJECT &raquo;</a></h5>
             </div>
          </div>

          
         <?php endwhile; endif; ?>
          <?php wp_reset_query(); ?>
</div>          
	
	