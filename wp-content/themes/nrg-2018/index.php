<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
	
	<?php require_once("includes/sidebar_links.php"); ?>	

		<div id="right">
			
			
			
			  <div class="pane2">
				<?php 
					$home = get_page_by_title('Home'); ?>
					<div class="pane_content">
						<?php echo $home->post_content; ?>
					</div>
					<?php echo get_the_post_thumbnail(841, 'large');
				?>
 			  </div>
 			  
				<?php get_sidebar(); ?>
				<?php get_footer(); ?>
				

	



