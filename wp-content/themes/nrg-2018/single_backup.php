<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php include("links.php"); ?>
		
		<div id="top"><?php include("tab_bar.php"); ?></div>
		
		<div id="right">

	<?php //get_sidebar(); ?>


	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


				<div id="post_single">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<div class="entry-meta">
						<?php if(in_category(4) && !is_single('events')) { the_time('l, M j, Y, g:i a'); } ?>
					</div><!-- .entry-meta -->
					

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						
			<?php 
				$title = get_the_title($post->ID);
				$safe_title = strtolower($title);
				$page = get_page_by_title($title);
					if ($page->post_title == $title) { ?>
						
						<a href="http://apux.org/nrg/project-index/<?php echo $safe_title; ?>">
									Click here to find out more about <?php the_title(); ?></a>
													
												<?php } ?>
				
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

		

<?php endwhile; // end of the loop. ?>

			
			
		<div id="right_single">
			<?php foreach((get_the_category()) as $category) { } ?>
			<h1 style="text-transform: uppercase; margin: 0px 20px 20px 20px;"><?php echo $category->category_nicename; ?></h1>

	<?php
		$ego = $post->ID;
		if($category->category_nicename == 'events') { 
			$args_now = array(
  				'category_name' => $category->category_nicename,
  				'post__not_in' => array(55, 57, 59, 61),
  				'post_status' => future,
  				'order' => ASC
  				);
 		} else { 
 			$args_now = array(
  				'category_name' => $category->category_nicename,
  				'post__not_in' => array(55, 57, 59, 61),
				'post_status' => array(published, future),
  				'order' => ASC 
  				);
		 }
  
 
   	 $my_query = new WP_Query($args_now);   
  	 while ($my_query->have_posts()) : $my_query->the_post();
 	

  		if( $post->ID == $ego) { ?>
   
       		<div class="post_current" onclick="window.location='<?php the_permalink() ?>'">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<h3><?php the_title(); ?></h3></a> <?php if(in_category(4)) { the_time('l, M j, Y, g:i a'); } ?>
					<?php the_excerpt(); ?>
		   	</div>
   		<?php } ?>

	<?php 
 
 		if($post->ID != $ego) { ?>  
    		
    		<div class="post_modern" onclick="window.location='<?php the_permalink() ?>'">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
					<h3><?php the_title(); ?></h3> </a> <?php if(in_category(4)) { the_time('l, M j, Y, g:i a'); } ?>
					<?php the_excerpt(); ?>				
		   </div>
  		<?php } ?>
  
  	<?php endwhile; ?>

  <?php if(in_category(4)) { ?>
  	<h4 style="float: right; margin: 50px 36px;"><a href="http://apux.org/nrg/event-archive" name="past events archive">View Archive of Past Events >></a></h4>
  <?php } ?>
</div>
<?php get_footer(); ?>

		</div>



