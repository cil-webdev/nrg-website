<?php 
/* 
** this is used to build sidebar navigation on all pages
*/
?>

<div id="page_side">
	  	<?php 
			
				global $post;
				$ancestors = get_post_ancestors($post->ID);
   			 	$reverse = array_reverse($ancestors);
   			 	$correct = get_the_title($reverse[0]);
   			 	$correct2 = get_the_title($reverse[1]);
   			 	$linker = get_permalink($reverse[1]);
   			 		if ( !empty($correct)) { 
   			 			$export = $correct;
   			 		} else { 
   			 			$export = $post->ID;
   			 		}
   			 		
   			 	echo "<h2><a href=\"" . $linker . "\">" . $correct2 . "</a></h2>";
   		?>
   		
	<?php 
			function project_subnavigation() {
				global $post;
				$ancestors = get_post_ancestors($post->ID);
   			 	$reverse = array_reverse($ancestors);
   			 	$correct = $reverse[1];
   			 		if ( !empty($correct)) { 
   			 			$export = $correct;
   			 		} else { 
   			 			$export = $post->ID;
   			 		}
   			 	return $export;
			}
    ?>
   
				<?php
					
				  if((get_the_title($post->ID) == "Software") OR (get_the_title($post->ID) == "Projects")) { 
			 		$children = wp_list_pages('depth=1&title_li= &child_of='.project_subnavigation()); 	
		   			 	echo $children; 
		   			 	} 
		   		  else { 
		   				$children = wp_list_pages('depth=9&title_li= &child_of='.project_subnavigation()); 	
		   			 	echo $children; 
		   			 	}
		   			 	
				?>

		

	
</div>