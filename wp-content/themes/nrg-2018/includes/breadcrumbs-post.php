<div id="breadcrumbs" style="margin-left: 10px;">
    <a href="<?php bloginfo('url'); ?>">Home</a> &raquo; 
    <?php foreach((get_the_category()) as $category) { } ?>
    <?php if($category->category_parent) { echo get_cat_name($category->category_parent) . "&raquo;"; } ?> 
    <a href="<?php bloginfo('url'); ?>/<?php echo $category->category_nicename; ?>"><?php echo $category->cat_name; ?></a> &raquo; 
    <?php  the_title(); ?>
</div><!--#breadcrumbs-->