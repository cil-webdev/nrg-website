<?php if(is_page() && !$post->post_parent) { ?>
    <div id="breadcrumbs" style="margin-left: 10px;">
        <a href="<?php bloginfo('url'); ?>">Home</a> &raquo; <?php the_title(); ?>
    </div>
<?php } else { ?>
    <div id="breadcrumbs">
        <a href="<?php bloginfo('url'); ?>">Home</a>&nbsp; &raquo;
        <a href="<?php bloginfo('url'); ?>/<?php echo strtolower($correct); ?>"><?php echo $correct; ?></a>
        <?php	 	
            krsort($ancestors);
            foreach($ancestors as $id2) { 
                $poste2 = get_page($id2);
                $title2 = get_the_title($poste2->ID);
                $link2 = get_permalink($poste2->ID);
                    if($title2 != $export) {
                        echo "&raquo; <a href=" . $link2 . ">" . $title2 . "</a>";
                    }
            }
            if($title2 != $reverse[1]) {	
                echo " &raquo; "; the_title(); } ?>
    </div>
<?php }?>