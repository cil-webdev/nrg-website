// JavaScript Document

$(document).ready(function ()
{
	// check to see if the page needs scroll bars -- need to subtract width if there aren't
	// need to use scrollHeight to make IE happy. 
	if (( $(document).height() > $(window).height() ) && (document.documentElement.scrollHeight > $(window).height() ))
	{
	//	do nothing if the page requires scrollbars. 
	} else {
		if ((navigator.appVersion.indexOf("Mac")!=-1) || (navigator.appVersion.indexOf("Linux")!=-1)) { 
		// change the setting for Mac
			$('#wrapper').css('padding-right', '26px');
			
		} else if (navigator.appVersion.indexOf("X11")!=-1) {
			// change the setting for Unix
			$('#wrapper').css('padding-right', '25px');
		
		} else if ((navigator.appVersion.indexOf("MSIE 6.0")!=-1) || (navigator.appVersion.indexOf("MSIE 7.0")!=-1)) {
			// do nothing for IE 6 or IE 7 ... has a scrollbar space on all pages
		} else {
			$('#wrapper').css('padding-right', '27px');
		}
	}
});

//on page load, hide all the panes within the panel, and empower any click on a "tab" to open the panel. 
$(document).ready(function ()
{
	$('.pane').addClass('hidden'); 
	$('.tab').click(function() {
		$('#panel').fadeIn();		
	});
});
 
//called by ahref link within each tab. ahref link specifies which pane (by ID) to work with. 
function paneswitch(pane) 
{
	if ( $('div#'+pane).is(':visible') ) {
		// is this pane already visible? If so, close the panel, and deactivate all panes within the panel. 
		panelclose(); 
	} else {
		// if not, show this pane, and hide its siblings. 
		$('div#'+pane).fadeIn(); 
		$('div#'+pane).siblings('.pane').fadeOut();
	}
}
 
// simple function to close the panel. Accessed in multiple ways. 
function panelclose() {
	$('#panel').fadeOut();
	$('.pane').fadeOut();
}
