<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
	<?php include("links.php"); ?>
	
		<div id="right">

<?php 
	$mySearch =& new WP_Query("s=$s & showposts=-1");
	$num = $mySearch->post_count;
	$num_cb = $wp_query->post_count;
	$id_cb = $paged;
	$r_cb=1;
	$startNum_cb = $r_cb;
	$endNum_cb = 10;
	if($id_cb >=2) {
		 $s_cb=$id_cb-1;
 		 $r_cb=($s_cb * 10) + 1;
 		 $startNum_cb=$r_cb;
  		 $endNum_cb=$startNum_cb + ($num_cb - 1);
	}
	if($num < 10){ $endNum_cb = $num; }
?>

<?php if ( have_posts() ) : ?>

			<div id="subpage">
				<h2><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<em>"' . get_search_query() . '"</em>' ); ?>
				
				<span><?php echo $startNum_cb . "-" . $endNum_cb; ?> of <?php echo $num; ?> results</span></h2>
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'search' );
				?>
				
			</div>
<?php else : ?>
				<div id="post">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
<?php endif; ?>


<?php get_footer(); ?>
		


