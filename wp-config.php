<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/html/nrg/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'nrg');

/** MySQL database password */
define('DB_PASSWORD', 'Z7l9dCn8sGM1aWcAKsXZ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S_+jDWeSj#<~&E+U:;HdLGnd^Q[BxT[A+sbL]m}z(jx85c2Z ?rvbkG+*b5Au~Tj');
define('SECURE_AUTH_KEY',  'd0L8{W} Y9qL71r[)l=rp`D;^nF5Eb2bk>`On,auGXP9s^f_ 6;ztzf- a%+EANU');
define('LOGGED_IN_KEY',    'a,-}IB4~4VE!8htd#BQab/@h2@W6g=`-{n@LX%Tmbrb2xCNm:2=/^!pdI6!8_(pX');
define('NONCE_KEY',        'I`7l2IM7WkDPLi#u!#k^Gt^OL|Ff;wK4}5$f~/[rfiG|&*$k.!uUK>|)!%]w-~LK');
define('AUTH_SALT',        'Mj]kPyS*7|dA4lT~?X%_fk}:*}WFd)1.`#)KvI|$>+geph|u$q_Fx8Z<IDMykf.5');
define('SECURE_AUTH_SALT', ';[?=3#Z[C3`e2XMFqQ6oF:qtPzLxeUfL>{i7-6Hdr^|S$WdJInBSU)dksc6CMGnw');
define('LOGGED_IN_SALT',   '[7H[*Cte-ogt(}FwW>R)R@xsfXcY3J3C0JylBa-bN3+#6BbtyTTRwMC,dvgj|L~@');
define('NONCE_SALT',       'dZU[C}p|)=l9eoyp<(^dr}}R7JA:pf|X~ 4.jD}Y@wBb!d~$|qQu:i}uV&E1O%#C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nrg_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Force all logins and all admin sessions to happen over SSL. */
define('FORCE_SSL_ADMIN', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

